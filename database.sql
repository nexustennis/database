-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 01, 2017 at 01:01 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nexustennisdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `friendID` int(16) NOT NULL,
  `user1` int(16) NOT NULL,
  `user2` int(16) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rankings`
--

CREATE TABLE `rankings` (
  `rankingID` int(16) NOT NULL,
  `rankingSystem` varchar(32) NOT NULL,
  `date` date NOT NULL,
  `playerName` varchar(128) NOT NULL,
  `ranking` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scoresAndStatsTrackerMatches`
--

CREATE TABLE `scoresAndStatsTrackerMatches` (
  `scoresAndStatsTrackerMatchID` int(16) NOT NULL,
  `numberOfSets` int(1) NOT NULL,
  `tiebreakAt` int(1) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `sessionID` int(16) NOT NULL,
  `uuid` varchar(64) NOT NULL,
  `contents` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`sessionID`, `uuid`, `contents`) VALUES
(12, '005b429a-e1e4-4e90-911b-601ac77fcf17', '{"loggedIn":true,"userID":1,"username":"The_TT_Hacker","firstname":"Tim","lastname":"Thacker","email":"timthacker97@gmail.com"}'),
(14, '5278c11c-eced-4ad5-93db-d4f1612ce50e', '{"loggedIn":true,"userID":1,"username":"The_TT_Hacker","firstname":"Tim","lastname":"Thacker","email":"timthacker97@gmail.com"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(16) NOT NULL,
  `username` varchar(32) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `email` varchar(320) NOT NULL,
  `password` varchar(60) NOT NULL,
  `profileType` varchar(64) NOT NULL,
  `permission` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `username`, `firstname`, `lastname`, `email`, `password`, `profileType`, `permission`) VALUES
(1, 'The_TT_Hacker', 'Tim', 'Thacker', 'timthacker97@gmail.com', '$2a$10$fWvCTotCqAp0cHWtOgogfuL0Tz5bD7n1eP3retaypsrAuy8GZkh3G', 'player', 'user'),
(10, 'andrewbennett1', 'Andrew', 'Bennett', 'andrewbennetennis@gmail.com', '$2a$10$ePbVt1jvApG7wAX3oIkFf.nZgvY9PXvIRKvqlOn5erocXMj4RsxhO', '["player","coach"]', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`friendID`);

--
-- Indexes for table `rankings`
--
ALTER TABLE `rankings`
  ADD PRIMARY KEY (`rankingID`);

--
-- Indexes for table `scoresAndStatsTrackerMatches`
--
ALTER TABLE `scoresAndStatsTrackerMatches`
  ADD PRIMARY KEY (`scoresAndStatsTrackerMatchID`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sessionID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`);
ALTER TABLE `users` ADD FULLTEXT KEY `search` (`username`,`firstname`,`lastname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `friendID` int(16) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rankings`
--
ALTER TABLE `rankings`
  MODIFY `rankingID` int(16) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scoresAndStatsTrackerMatches`
--
ALTER TABLE `scoresAndStatsTrackerMatches`
  MODIFY `scoresAndStatsTrackerMatchID` int(16) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `sessionID` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
